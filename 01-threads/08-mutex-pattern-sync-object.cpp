#include <mutex>
#include <thread>
#include <iostream>

class WrongCounter {
public:
    void Update(int value) {
        sum_ += value;
        count_ += 1;
    }

    int GetAvg() { return sum_ / count_; }

private:
    int64_t sum_ = 0;
    size_t count_ = 0;
};


int main() {
    WrongCounter c;

    std::thread t1([&c] {
        for (int i = 0; i < 1024; ++i) {
            c.Update(1000000);
        }
    });

    std::thread t2([&c] {
        for (int i = 0; i < 1024; ++i) {
            c.Update(2000000);
        }
    });

    t1.join();
    t2.join();

    std::cout << c.GetAvg() << std::endl;;

    return 0;
}
