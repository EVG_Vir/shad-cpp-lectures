#include <thread>
#include <sstream>
#include <iostream>
#include <exception>

void DoWork() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

int main() {
    try {
        std::thread worker(DoWork);

        throw std::runtime_error("Other part of the program failed");

        worker.join(); // same problem, as delete + naked pointer
    } catch (const std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
